'use strict';

const express = require('express');
var firebase = require('firebase');
var bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

var config = {
    apiKey: "AIzaSyAmW10pWeKa4tXiPZay6QWcFTElvDW7chA",
    authDomain: "mulpst.firebaseapp.com",
    databaseURL: "https://mulpst.firebaseio.com",
    projectId: "mulpst",
    storageBucket: "mulpst.appspot.com",
    messagingSenderId: "687825958666"
};
firebase.initializeApp(config);

app.get('/', (req, res) => {
    var datasRef = firebase.database().ref("/Datas");

    datasRef.on("value",
        function(snapshot) {
            res.json(snapshot.val());
            datasRef.off("value");
        },
        function(errorObject){
            res.send(errorObject.code);
        });
});

app.post('/', function(req, res){
   var b0 = req.body.B0;
   var b1 = req.body.B1;
   var b2 = req.body.B2;
   var b3 = req.body.B3;
   var b4 = req.body.B4;
   var b5 = req.body.B5;
   
   var t0 = req.body.T0;
   var t1 = req.body.T1;
   var t2 = req.body.T2;
   var t3 = req.body.T3;
   var t4 = req.body.T4;
   var t5 = req.body.T5;
   
   var referencePath = '/Datas';
   var userReference = firebase.database().ref(referencePath);
   userReference.update({B0: b0, B1: b1, B2: b2, B3: b3, B4: b4, B5: b5,
        T0: t0, T1: t1, T2: t2, T3: t3, T4: t4, T5: t5 },
      function(error){
          if (error){
              res.send("Data could not be updated!" + error);
          } 
          else{
              res.send("Data updated successfully!");  
          }
      });
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log('App listening on port ${PORT}');
});